package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Folder(id: Option[Int], title: String, userId: Int)

object Folders {

  private[Folders] val row = int("id") ~ str("title") ~ int("user_id") map {
    case id~title~userId => Folder(Option(id), title, userId)
  }

  def create(id: Int, title: String, userId: Int) { DB.withConnection { implicit connection =>
    SQL("insert into `folders` values({id}, {title}, {user_id})")
      .on("id" -> id, "title" -> title, "user_id" -> userId).execute()
  }}

  def findByUser(id: Int): Seq[Folder] = DB.withConnection { implicit conection =>
    SQL("select * from `folders` where user_id = {id}").on("id" -> id).as(row *)
  }


}