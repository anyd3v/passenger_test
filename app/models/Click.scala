package models

import java.util.Date

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Click(id: Option[Int], date: Date, referrer: String, ip: String, linkId: Int)

object Clicks {

  private[Clicks] val row = int("id") ~ date("date") ~ str("referrer") ~ str("ip") ~ int("link_id") map {
    case id~date~referrer~ip~linkId => Click(Option(id), date, referrer, ip, linkId)
  }

  def create(id: Option[Int], date: Date, referrer: String, ip: String, linkId: Int) = DB.withConnection { implicit connection =>
     SQL("insert into `clicks` values({id}, {referrer}, {ip}, {link_id}, {date})")
      .on("id" -> id, "date" -> date, "referrer" -> referrer, "ip" -> ip, "link_id" -> linkId).execute()
  }

  def findByLinkId(linkId: Int, limit: Int, offset: Int) = DB.withConnection { implicit  connection =>
    SQL("select * from `clicks` where link_id = {link_id} limit {offset},{limit}")
      .on("link_id" -> linkId, "limit" -> limit, "offset" -> offset).as(row *)
  }

}
