package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class User(id: Id[Int], token: String)

object Users {

  private[Users] val row = int("id") ~ str("token") map {
    case id~token => User(Id(id), token)
  }

  def create(id: Int, token: String) { DB.withConnection { implicit conection =>
    SQL("""insert into `users` values({id}, {token})""")
      .on("id" -> id, "token" -> token).execute()
  }}

  def findById(id: Int) = DB.withConnection { implicit conection =>
     SQL("select * from `users` where id = {id}").on("id" -> id).as(row.singleOpt)
  }

  def findByToken(token: String) = DB.withConnection { implicit conection =>
    SQL("select * from `users` where token = {token}").on("token" -> token).as(row.singleOpt)
  }

}
