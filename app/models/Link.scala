package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Link(id: Option[Int], url: String, code: String, userId: Int, folderId: Option[Int])

object Links {

  private[Links] val row = int("id") ~ str("url") ~ str("code") ~ int("user_id") ~ get[Option[Int]]("folder_id") map {
    case id~url~code~userId~folderId => Link(Option(id), url, code, userId, folderId)
  }

  private[Links] val rowWithCount = row ~ int("count")

  def create(id: Option[Int], url: String, code: String, userId: Int, folderId: Option[Int]) { DB.withConnection { implicit conection =>
    SQL("""insert into `links` values({id}, {url}, {code}, {user_id}, {folder_id})""")
      .on("id" -> id, "url" -> url, "code" -> code, "user_id" -> userId, "folder_id" -> folderId).execute()
  }}

  def findById(id: Int) = DB.withConnection { implicit connection =>
    SQL("select * from `links` where id = {id}").on("id" -> id).as(row.singleOpt)
  }

  def findByUserId(userId: Int, limit: Int, offset: Int) = DB.withConnection { implicit connection =>
    SQL("select * from `links` where user_id = {user_id} limit {offset},{limit}")
      .on("user_id" -> userId, "limit" -> limit, "offset" -> offset).as(row *)
  }

  def findByUserIdAndFolderId(userId: Int, folderId: Int, limit: Int, offset: Int) = DB.withConnection { implicit connection =>
    SQL("select * from `links` where user_id = {user_id} and folder_id = {folder_id} limit {offset},{limit}")
      .on("user_id" -> userId, "folder_id" -> folderId,  "limit" -> limit, "offset" -> offset).as(row *)
  }

  def findByCode(code: String) = DB.withConnection { implicit connection =>
    SQL("select * from `links` where code = {code}").on("code" -> code).as(row.singleOpt)
  }

  def findByUserIdAndCodeWithCount(code: String, userId: Int) = DB.withConnection { implicit connection =>
    SQL("select *, (select count(*) from `clicks` where `link_id` = `links`.`id`) as count from `links` where user_id = {user_id} and code = {code}")
      .on("code" -> code, "user_id" -> userId).as(rowWithCount.singleOpt).map(r => (r._1, r._2))
  }

  def findByUserIdAndCode(code: String, userId: Int) = DB.withConnection { implicit connection =>
    SQL("select * from `links` where user_id = {user_id} and code = {code}")
      .on("code" -> code, "user_id" -> userId).as(row.singleOpt)
  }

}