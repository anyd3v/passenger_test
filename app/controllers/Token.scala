package controllers

import models.Users
import play.Play
import play.api.libs.json._
import play.api.mvc._
import scala.collection.JavaConverters._

import scala.concurrent.Future


object Token extends Controller {

  implicit val tokenResponseWrites = Json.writes[TokenResponse]

  val secrets = Play.application().configuration().getStringList("auth.secrets").asScala

  def index = Action.async { implicit request =>
    Future successful {
      request.getQueryString("secret") -> request.getQueryString("id") match {
        case (Some(secret), Some(id)) =>
          secrets.find(_ == secret).map { _ =>
            Users.findById(id.toInt).map { user =>
              Ok(Json.toJson(TokenResponse(user.token)))
            } getOrElse NotFound
          } getOrElse BadRequest
        case _ => BadRequest
      }
    }
  }
}

case class TokenResponse(token: String)
