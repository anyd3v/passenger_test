package controllers

import java.util.Date

import models.{Clicks, Links}
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.Json
import play.api.mvc._

import scala.util.Try

object Link extends Controller with StatelessAuthController {

  implicit val linkItemWriter = Json.writes[LinkResponse]
  implicit val linkWriter = Json.writes[LinkInfoResponse]
  implicit val linkStatisticWriter = Json.writes[LinkStatisticResponse]

  val createLinkForm = Form {
    mapping("url" -> nonEmptyText, "code" -> nonEmptyText, "folder" -> optional(text))
    { (url, code, folder) => (url, code, folder) }
    { params => Some(params._1, params._2, params._3) }
  }

  val createClickForm = Form {
    mapping("referrer" -> nonEmptyText, "remote_ip" -> nonEmptyText)
    { (referrer, ip) => (referrer, ip) }
    { params => Some(params._1, params._2) }
  }

  def create = withAuth { userId => implicit request =>
    createLinkForm.bindFromRequest.fold(
      hasErrors => BadRequest,
      link => {
        Links.create(None, link._1, link._2, userId, link._3.map(f => Try(f.toInt).toOption).getOrElse(None))
        Created
      }
    )
  }

  def getForUser = withAuth { userId => implicit request =>
    val (limit, offset) = extractLimitOffset(request, 10, 0)
    val links = Links.findByUserId(userId, limit, offset)

    Ok(Json.toJson(links.map(l => LinkResponse(l.id, l.url, l.code))))
  }

  def getForFolder(folderId: Int) = withAuth { userId => implicit request =>
    val (limit, offset) = extractLimitOffset(request, 10, 0)
    val links = Links.findByUserIdAndFolderId(userId, folderId, limit, offset)

    Ok(Json.toJson(links.map(l => LinkResponse(l.id, l.url, l.code))))
  }

  def postForCode(code: String) = Action { implicit request =>
    createClickForm.bindFromRequest.fold(
      hasErrors => BadRequest,
      form => {
        Links.findByCode(code).map { link =>
          Clicks.create(None, new Date(), form._1, form._2, link.id.getOrElse(0))
          Ok(Json.toJson(LinkInfoResponse(link.url)))
        } getOrElse NotFound
      }
    )
  }

  def getForCode(code: String) = withAuth { userId => implicit request =>
    Links.findByUserIdAndCodeWithCount(code, userId).map { case(link, count) =>
       Ok(Json.toJson(LinkStatisticResponse(link.id, link.url, link.code, link.folderId, count)))
    } getOrElse NotFound
  }

}

case class LinkResponse(id: Option[Int], url: String, code: String)

case class LinkInfoResponse(url: String)

case class LinkStatisticResponse(id: Option[Int], url: String, code: String, folder: Option[Int], count: Int)
