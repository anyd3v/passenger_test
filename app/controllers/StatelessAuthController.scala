package controllers

import models._
import play.api.mvc._

trait StatelessAuthController {
  self: Controller =>

  def extractToken(request: RequestHeader): Option[String] = {
    Seq(
      request.cookies.get("token").map(_.value),
      request.getQueryString("token")
    ).flatten.headOption
  }

  def extractSession(request: Request[Any]): Option[User] = {
    extractToken(request) match {
      case Some(token) => Users.findByToken(token)
      case _ => None
    }
  }

  def onUnauthorized(request: RequestHeader): Result = Unauthorized("Provide token for authorization")

  def withAuth(f: => Int => Request[AnyContent] => Result): Action[AnyContent] =
    withAuth(BodyParsers.parse.anyContent)(f)

  def withAuth[A](p: BodyParser[A])(f: => Int => Request[A] => Result): Action[A] =
    Action(p)(request => extractSession(request).map(user =>  f(user.id)(request)) getOrElse onUnauthorized(request))

}
