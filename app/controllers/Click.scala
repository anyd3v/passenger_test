package controllers

import models.{Clicks, Links}
import play.api.libs.json.Json
import play.api.mvc._

object Click extends Controller with StatelessAuthController {

  implicit val clickItemWrites = Json.writes[ClickResponseItem]

  def getLinksClicks(code: String) = withAuth { userId => implicit request =>
    Links.findByUserIdAndCode(code, userId).map { link =>
      link.id.map { linkId =>
        val (limit, offset) = extractLimitOffset(request, 10, 0)
        val clicks = Clicks.findByLinkId(link.id.getOrElse(-1), limit, offset)
        Ok(Json.toJson(clicks.map(c => ClickResponseItem(c.id, c.referrer, c.ip))))
      } getOrElse NotFound
    } getOrElse NotFound
  }

}

case class ClickResponseItem(id: Option[Int], referrer: String, ip: String)
