import play.api.mvc.Request

package object controllers {

  def extractLimitOffset(request: Request[Any], defaultLimit: Int, defaultOffset: Int): (Int, Int) =
    request.getQueryString("limit").map(_.toInt).getOrElse(defaultLimit) ->
      request.getQueryString("offset").map(_.toInt).getOrElse(defaultOffset)

}
