package controllers

import models.Folders
import play.api.libs.json.Json
import play.api.mvc.Controller

object Folder extends Controller with StatelessAuthController {

  implicit val foldersWrites = Json.writes[UserFolders]

  def getForUser = withAuth { userId => implicit request =>
    val folders = Folders.findByUser(userId)
    Ok(Json.toJson(folders.map(f => UserFolders(f.id.getOrElse(-1), f.title))))
  }

}

case class UserFolders(id: Int, title: String)
