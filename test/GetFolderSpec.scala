import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

import play.api.test._
import play.api.test.Helpers._

@RunWith(classOf[JUnitRunner])
class GetFolderSpec extends Specification with WithDatabase with DbPopulation {

  "Application" should {

    def getFoldersRequest(token: String) = {
      route(FakeRequest(GET, s"/folder?token=${token}"))
    }

    "return list of folders for user" in inMemory {
      populateUser(1, "token")
      populateFolder(1, "title", 1)

      val getFoldersResponse = getFoldersRequest("token").get

      status(getFoldersResponse) must equalTo(OK)
      contentAsString(getFoldersResponse) must equalTo ("""[{"id":1,"title":"title"}]""")
    }

    "return list of folders for expect user" in inMemory {
      populateUser(1, "token1")
      populateFolder(1, "title1", 1)
      populateUser(2, "token2")
      populateFolder(2, "title2", 2)

      val getFoldersResponse = getFoldersRequest("token2").get

      status(getFoldersResponse) must equalTo(OK)
      contentAsString(getFoldersResponse) must equalTo ("""[{"id":2,"title":"title2"}]""")
    }

  }

}
