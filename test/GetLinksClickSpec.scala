import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class GetLinksClickSpec extends Specification with WithDatabase with DbPopulation {

    "Application" should {

      def getLinksClicksRequest(token: String, code: String) = {
        route(FakeRequest(GET, s"/link/${code}/clicks?token=${token}"))
      }

      def getLinksClicksRequestWithLimitOffset(token: String, code: String, limit: Int, offset: Int) = {
        route(FakeRequest(GET, s"/link/${code}/clicks?token=${token}&limit=${limit.toString}&offset=${offset}"))
      }

      "return correct clicks" in inMemory {
        populateUser(1, "token")
        populateLink(Option(1), "url1", "code1", 1, None)
        populateClick(Option(1), "http://referrer", "192.168.1.1", 1)

        val clicksResponse = getLinksClicksRequest("token", "code1").get

        status(clicksResponse) must equalTo(OK)
        contentAsString(clicksResponse) must equalTo ("""[{"id":1,"referrer":"http://referrer","ip":"192.168.1.1"}]""")
      }


      "return correct clicks for other user" in inMemory {
        populateUser(1, "token1")
        populateUser(2, "token2")
        populateLink(Option(1), "url1", "code1", 1, None)
        populateClick(Option(1), "http://referrer", "192.168.1.1", 1)

        val clicksResponse = getLinksClicksRequest("token2", "code1").get

        status(clicksResponse) must equalTo(NOT_FOUND)
      }

      "return correct clicks for custom limit,offset" in inMemory {
        populateUser(1, "token")
        populateLink(Option(1), "url1", "code1", 1, None)
        populateClick(Option(1), "http://1", "1", 1)
        populateClick(Option(2), "http://2", "2", 1)

        val clicksResponse1 = getLinksClicksRequestWithLimitOffset("token", "code1", 1, 0).get

        status(clicksResponse1) must equalTo(OK)
        contentAsString(clicksResponse1) must equalTo ("""[{"id":1,"referrer":"http://1","ip":"1"}]""")

        val clicksResponse2 = getLinksClicksRequestWithLimitOffset("token", "code1", 1, 1).get

        status(clicksResponse2) must equalTo(OK)
        contentAsString(clicksResponse2) must equalTo ("""[{"id":2,"referrer":"http://2","ip":"2"}]""")

        val clicksResponse3 = getLinksClicksRequestWithLimitOffset("token", "code1", 1, 2).get

        status(clicksResponse3) must equalTo(OK)
        contentAsString(clicksResponse3) must equalTo ("""[]""")
      }

    }
}
