import play.api.test.FakeApplication
import play.api.test.Helpers._

trait WithDatabase {

  def inMemory[T](code: => T) = running(FakeApplication(additionalConfiguration = inMemoryDatabase()))(code)

}
