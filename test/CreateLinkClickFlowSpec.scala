import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

class CreateLinkClickFlowSpec extends Specification with WithDatabase with DbPopulation {

  "Application" should {

    "create click when get url by code" in inMemory {
      populateUser(1, "token")
      populateLink(Option(1), "http://url", "code", 1, None)

      val postLink = route(FakeRequest(POST, "/link/code")
        .withFormUrlEncodedBody("referrer" -> "http://referrer", "remote_ip" -> "192.168.1.1")).get

      status(postLink) must equalTo(OK)

      val getClicks = route(FakeRequest(GET, "/link/code/clicks?token=token")).get

      status(getClicks) must equalTo(OK)
      contentAsString(getClicks) must equalTo ("""[{"id":1,"referrer":"http://referrer","ip":"192.168.1.1"}]""")
    }

  }

}
