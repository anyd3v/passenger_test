import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._

@RunWith(classOf[JUnitRunner])
class GetTokenSpec extends Specification with WithDatabase with DbPopulation {

  "Application" should {

    def getTokenRequest(id: Int, secret: String) = {
      route(FakeRequest(GET, s"/token?id=${id.toString}&secret=$secret"))
    }

    "return token for valid params" in inMemory {
      populateUser(1, "token")
      val takeTokenRequest = getTokenRequest(1, "anyhardcodedsecrets").get

      status(takeTokenRequest) must equalTo(OK)
      contentAsString(takeTokenRequest) must equalTo ("""{"token":"token"}""")
    }

    "return 400 for request without params" in inMemory {
      populateUser(1, "token")
      val takeTokenRequest = route(FakeRequest(GET, "/token")).get

      status(takeTokenRequest) must equalTo(BAD_REQUEST)
    }

    "return 400 for request with invalid secret" in inMemory {
      populateUser(1, "token")
      val takeTokenRequest = getTokenRequest(1, "invalid_token").get

      status(takeTokenRequest) must equalTo(BAD_REQUEST)
    }

    "return 404 for request with invalid id" in inMemory {
      populateUser(1, "token")
      val takeTokenRequest = getTokenRequest(2, "anyhardcodedsecrets").get

      status(takeTokenRequest) must equalTo(NOT_FOUND)
    }

  }



 }
