import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class CreateLinkSpec extends Specification with WithDatabase with DbPopulation {

   "Application" should {

     def createLinkRequest(token: String, url: String, code: String) = {
       route(FakeRequest(POST, s"/link?token=${token}").withFormUrlEncodedBody("url" -> url, "code" -> code))
     }

     "create link for valid params" in inMemory {
       populateUser(1, "token")

       val getFoldersResponse = createLinkRequest("token", "http://google.com", "google").get

       status(getFoldersResponse) must equalTo(CREATED)
     }

     "create link must return 400 for invalid parameters (empty form)" in inMemory {
       populateUser(1, "token")

       val getFoldersResponse = route(FakeRequest(POST, "/link?token=token")).get

       status(getFoldersResponse) must equalTo(BAD_REQUEST)
     }

     "create link must return 400 for invalid parameters (without code)" in inMemory {
       populateUser(1, "token")

       val getFoldersResponse = route(FakeRequest(POST, s"/link?token=token").withFormUrlEncodedBody("url" -> "test")).get

       status(getFoldersResponse) must equalTo(BAD_REQUEST)
     }

     "create link must return 400 for invalid parameters (without url)" in inMemory {
       populateUser(1, "token")

       val getFoldersResponse = route(FakeRequest(POST, s"/link?token=token").withFormUrlEncodedBody("code" -> "test")).get

       status(getFoldersResponse) must equalTo(BAD_REQUEST)
     }


   }

 }
