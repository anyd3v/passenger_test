import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class PostLinkByCodeSpec extends Specification with WithDatabase with DbPopulation {

  "Application" should {

    def postLinkByCode(code: String, referrer: Option[String], ip: Option[String]) = {
      val form: Map[String, String] = Map("referrer" -> referrer, "remote_ip" -> ip).foldLeft[Map[String, String]](Map())((map, el) =>
        el match {
          case (key, Some(value)) => map + (key -> value)
          case _ => map
        }
      )
      route(FakeRequest(POST, s"/link/${code}").withFormUrlEncodedBody(form.toList:_*))
    }

    "return url by code" in inMemory {
      populateUser(1, "token")
      populateLink(Option(1), "http://google.com", "code", 1, None)

      val postLinkResponse = postLinkByCode("code", Option("http://some_referrer"), Option("127.0.0.1")).get

      status(postLinkResponse) must equalTo(OK)
      contentAsString(postLinkResponse) must equalTo ("""{"url":"http://google.com"}""")
    }

    "return 400 for invalid post" in inMemory {
      populateUser(1, "token")
      populateLink(Option(1), "http://google.com", "code", 1, None)

      val postLinkResponse1 = postLinkByCode("code", None, Option("127.0.0.1")).get

      status(postLinkResponse1) must equalTo(BAD_REQUEST)

      val postLinkResponse2 = postLinkByCode("code", Option("http://some_referrer"), None).get

      status(postLinkResponse2) must equalTo(BAD_REQUEST)

      val postLinkResponse3 = postLinkByCode("code", None, None).get

      status(postLinkResponse3) must equalTo(BAD_REQUEST)
    }

  }

}
