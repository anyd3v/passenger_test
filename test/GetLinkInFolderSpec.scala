import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class GetLinkInFolderSpec extends Specification with WithDatabase with DbPopulation {

   "Application" should {

     def getLinksInFolderRequest(token: String, folderId: Int) = {
       route(FakeRequest(GET, s"/folder/${folderId.toString}?token=${token}"))
     }

     def getLinksInFolderRequestWithLimitOffset(token: String, folderId: Int, limit: Int, offset: Int) = {
       route(FakeRequest(GET, s"/folder/${folderId.toString}?token=${token}&limit=${limit.toString}&offset=${offset}"))
     }

     "return list of links in folder" in inMemory {
       populateUser(1, "token")
       populateFolder(1, "folder", 1)
       populateLink(Option(1), "http://google.com", "secretcode", 1, Option(1))

       val getLinksResponse = getLinksInFolderRequest("token", 1).get

       status(getLinksResponse) must equalTo(OK)
       contentAsString(getLinksResponse) must equalTo ("""[{"id":1,"url":"http://google.com","code":"secretcode"}]""")
     }

     "return list of links in folder" in inMemory {
       populateUser(1, "token1")
       populateFolder(1, "folder1", 1)
       populateLink(Option(1), "http://google.com", "secretcode", 1, Option(1))
       populateUser(2, "token2")
       populateFolder(2, "folder2", 2)
       populateLink(Option(2), "http://yandex.ru", "notsecretcode", 2, Option(2))

       val getLinksResponse = getLinksInFolderRequest("token2", 2).get

       status(getLinksResponse) must equalTo(OK)
       contentAsString(getLinksResponse) must equalTo ("""[{"id":2,"url":"http://yandex.ru","code":"notsecretcode"}]""")
     }

     "return list of links in folder for custom offset,limit" in inMemory {
       populateUser(1, "token")
       populateFolder(1, "folder1", 1)
       populateLink(Option(1), "http://google.com", "secretcode", 1, Option(1))
       populateLink(Option(2), "http://yandex.ru", "notsecretcode", 1, Option(1))

       val getLinksResponse1 = getLinksInFolderRequestWithLimitOffset("token", 1, 1, 1).get

       status(getLinksResponse1) must equalTo(OK)
       contentAsString(getLinksResponse1) must equalTo ("""[{"id":2,"url":"http://yandex.ru","code":"notsecretcode"}]""")

       val getLinksResponse2 = getLinksInFolderRequestWithLimitOffset("token", 1, 1, 0).get

       status(getLinksResponse2) must equalTo(OK)
       contentAsString(getLinksResponse2) must equalTo ("""[{"id":1,"url":"http://google.com","code":"secretcode"}]""")
     }

   }

 }
