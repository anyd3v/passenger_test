import java.util.Date

import models._

trait DbPopulation {

  def populateUser(id: Int, token: String) { Users.create(id, token) }

  def populateFolder(id: Int, title: String, userId: Int) { Folders.create(id, title, userId) }

  def populateLink(id: Option[Int], url: String, code: String, userId: Int, folderId: Option[Int]) {
    Links.create(id, url, code, userId, folderId)
  }

  def populateClick(id: Option[Int], referrer: String, ip: String, linkId: Int) {
    Clicks.create(id, new Date(), referrer, ip, linkId)
  }

}
