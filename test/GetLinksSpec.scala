import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class GetLinksSpec extends Specification with WithDatabase with DbPopulation {

   "Application" should {

     def getLinksRequest(token: String) = {
       route(FakeRequest(GET, s"/link?token=${token}"))
     }

     def getLinksRequestWithLimitOffset(token: String, limit: Int, offset: Int) = {
       route(FakeRequest(GET, s"/link?token=${token}&limit=${limit.toString}&offset=${offset}"))
     }

     "return list of links for user" in inMemory {
       populateUser(1, "token")
       populateLink(Option(1), "http://google.com", "secretcode", 1, None)

       val getLinksResponse = getLinksRequest("token").get

       status(getLinksResponse) must equalTo(OK)
       contentAsString(getLinksResponse) must equalTo ("""[{"id":1,"url":"http://google.com","code":"secretcode"}]""")
     }

     "return list of links for expect user" in inMemory {
       populateUser(1, "token1")
       populateLink(Option(1), "http://google.com", "secretcode", 1, None)
       populateUser(2, "token2")
       populateLink(Option(2), "http://yandex.ru", "notsecretcode", 2, None)

       val getLinksResponse = getLinksRequest("token2").get

       status(getLinksResponse) must equalTo(OK)
       contentAsString(getLinksResponse) must equalTo ("""[{"id":2,"url":"http://yandex.ru","code":"notsecretcode"}]""")
     }

     "return list of links correct work for custom offset,limit" in inMemory {
       populateUser(1, "token")
       populateLink(Option(1), "http://google.com", "secretcode", 1, None)
       populateLink(Option(2), "http://yandex.ru", "notsecretcode", 1, None)

       val getLinksResponse1 = getLinksRequestWithLimitOffset("token", 1, 1).get

       status(getLinksResponse1) must equalTo(OK)
       contentAsString(getLinksResponse1) must equalTo ("""[{"id":2,"url":"http://yandex.ru","code":"notsecretcode"}]""")

       val getLinksResponse2 = getLinksRequestWithLimitOffset("token", 1, 0).get

       status(getLinksResponse2) must equalTo(OK)
       contentAsString(getLinksResponse2) must equalTo ("""[{"id":1,"url":"http://google.com","code":"secretcode"}]""")
     }

   }

 }
