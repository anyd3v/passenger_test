import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

@RunWith(classOf[JUnitRunner])
class GetLinkInfoSpec extends Specification with WithDatabase with DbPopulation {

  "Application" should {

    def getLinkInfoRequest(token: String, code: String) = {
      route(FakeRequest(GET, s"/link/${code}?token=${token}"))
    }

    "return correct link info" in inMemory {
      populateUser(1, "token")
      populateFolder(10, "folder", 1)
      populateLink(Option(1), "url1", "code1", 1, Option(10))
      populateClick(Option(1), "referrer", "ip", 1)
      populateClick(Option(2), "referrer", "ip", 1)

      val getLinkInfoResponse = getLinkInfoRequest("token", "code1").get

      status(getLinkInfoResponse) must equalTo(OK)
      contentAsString(getLinkInfoResponse) must equalTo ("""{"id":1,"url":"url1","code":"code1","folder":10,"count":2}""")
    }

    "return 404 for not exist link" in inMemory {
      populateUser(1, "token")

      val getLinkInfoResponse = getLinkInfoRequest("token", "code1").get

      status(getLinkInfoResponse) must equalTo(NOT_FOUND)
    }

  }

}
