# ---

# --- !Ups

CREATE TABLE `users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `token_UNIQUE` (`token` ASC))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `folders` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(256) NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `folder_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `links` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(4000) NOT NULL,
  `code` VARCHAR(128),
  `user_id` INT UNSIGNED NOT NULL,
  `folder_id` INT UNSIGNED,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  CONSTRAINT `link_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `clicks` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `referrer` VARCHAR(4000) NOT NULL,
  `ip` VARCHAR(15) NOT NULL,
  `link_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `click_link_id_fk`
  FOREIGN KEY (`link_id`)
  REFERENCES `links` (`id`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;


# --- !Downs

